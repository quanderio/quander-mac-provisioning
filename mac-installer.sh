POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--password)
    PASSWORD="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ -z "${PASSWORD}" ]]; then
    echo "ERROR: Missing user password, -p, --password <value>"
    exit
fi

printf "Quander"
sleep 0.5
printf "\rQuander Mac"
sleep 0.5
printf "\rQuander Mac Setter"
sleep 0.5
printf "\rQuander Mac Setter-upper"
sleep 0.5
printf "\rQuander Mac Setter-upper 🛠"
sleep 0.5

echo ""
echo ""

echo "🛠  - Installing Homebrew"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)";

echo "🛠  - Homebrew installs"
brew install brew-cask
brew install nvm
brew install beanstalkd
brew install ola
brew install wallpaper

brew cleanup

echo "⚙️  - System - Start beanstalkd on system start up"
brew services start beanstalkd

echo "🛠  - Brew cask installs"
brew cask install google-chrome
brew cask install vlc
brew cask install iterm2
brew cask install logmein-hamachi
brew cask install caffeine
brew cask install quicklook-json
brew cask install qlmarkdown
brew cask install visual-studio-code

mkdir ~/.nvm
echo 'export NVM_DIR=~/.nvm' >>~/.bash_profile
echo 'source $(brew --prefix nvm)/nvm.sh' >>~/.bash_profile

source ~/.bash_profile
echo $NVM_DIR

# Install latest Node.js
nvm install stable && nvm use stable
nvm alias default stable

echo "🛠  - Installing beanmaster"
npm install -g beanmaster

echo "😈  - Starting beanmaster daemon"
beanmaster start

echo "⚙️  - System - Start LogMeIn Hamachi at start up"
sudo launchctl load -w /Library/LaunchDaemons/com.logmein.logmeinserver.plist

echo "⚙️  - System - Automatically restart if system freezes"
sudo systemsetup -setrestartfreeze on

echo "⚙️  - System - Avoid creating .DS_Store files on network volumes"
sudo defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

echo "⚙️  - System - Disable automatically check for updates"
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled -bool false

echo "⚙️  - System - Disable crash reporter dialog"
sudo defaults write com.apple.CrashReporter DialogType none
sudo defaults write com.apple.CrashReporter UseUNC 1

echo "⚙️  - System - Disable application resume on restart."
sudo defaults write com.apple.systempreferences NSQuitAlwaysKeepsWindows -bool false
sudo defaults write -g ApplePersistenceIgnoreState YES

echo "⚙️  - System - Disable warning on opening new app."
sudo spctl --master-disable
sudo defaults write com.apple.LaunchServices LSQuarantine -bool false

echo "⚙️  - System - Disable Bluetooth seeking keyboards"
sudo defaults write /Library/Preferences/com.apple.Bluetooth BluetoothAutoSeekKeyboard '0'

echo "⚙️  - System - Disable Bluetooth seeking mice"
sudo defaults write /Library/Preferences/com.apple.Bluetooth BluetoothAutoSeekPointingDevice '0'

echo "⚙️  - System - Disable screensaver"
defaults write com.apple.screensaver idleTime -int 0
defaults -currentHost write com.apple.screensaver idleTime -int 0

echo "⚙️  - System - Disable system sleep"
sudo pmset sleep 0

echo "⚙️  - System - Disable display sleep"
sudo pmset displaysleep 0

echo "⚙️  - System - Enable wake on ethernet"
sudo pmset womp 1

echo "⚙️  - System - Enable remote login"
sudo systemsetup -setremotelogin on

echo "⚙️  - System - Enable remote management"
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on -users admin -privs -all -restart -agent -menu

echo "⚙️  - Finder - Show the $HOME/Library folder"
chflags nohidden $HOME/Library

echo "⚙️  - Finder - Show filename extensions"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

echo "⚙️  - Finder - Show path bar"
defaults write com.apple.finder ShowPathbar -bool true

echo "⚙️  - Finder - Show status bar"
defaults write com.apple.finder ShowStatusBar -bool true

echo "⚙️  - Disable Photos.app from starting everytime a device is plugged in"
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

echo "⚙️  - System - Auto login admin"
brew tap xfreebird/utils
brew install kcpassword
enable_autologin $USER $PASSWORD

echo "⚙️  - System - Set desktop wallpaper"
curl https://bitbucket.org/quanderio/quander-mac-provisioning/raw/88dce99acfa94531b33bab4c28ddb78f18a29b08/background.jpg > Pictures/background.jpg && wallpaper set Pictures/background.jpg

echo "⚙️  - System - Secondary click"
sudo defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseButtonMode -string 'TwoButton'
sudo defaults write com.apple.driver.AppleHIDMouse Button2 -int 2

echo "⚙️  - Finder - Remove all apps from the Dock"
defaults write com.apple.dock persistent-apps -array

killall Dock

# echo "⚙️  - Finder - Add apps to the Dock"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Safari.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Google Chrome.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/LogMeIn Hamachi/LogMeIn Hamachi.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Visual Studio Code.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTerm.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Utilities/Console.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/System Preferences.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

killall Dock
killall Finder

echo ""
echo ""

wait_time=10 # seconds
temp_cnt=${wait_time}
while [[ ${temp_cnt} -gt 0 ]];
do
    printf "\r🚀  System will shutdown and restart in %2d second(s). Hit Ctrl+C to cancel." ${temp_cnt}
    sleep 1
    ((temp_cnt--))
done
echo ""
echo ""
echo "Shutting down and restarting"

sudo shutdown -r now