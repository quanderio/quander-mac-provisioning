# Quander Mac Provisioning 
## Running
```
bash <(curl -s https://storage.googleapis.com/quander-mac-provisioning/mac-installer.sh) --password <ACCOUNT_PASSWORD>
```

### Parameters
#### Password
`-p` OR `--password` of the currently logged in user to setup automatic login.

## Todo list / additional setup requirements checklist
- [ ] Disable NotificationCenter
- [ ] Install Power JSON Editor.app
- [ ] Add TeamViewer to Accessibility Preferences for remote access (will require disabling and re-enabling SIP).
- [ ] Install LaunchControl.app / Add our apps to `launchctl` manually.
- [ ] Install Meraki profiles
- [ ] Install Blackmagic Drivers
- [ ] Static link to `mac-installer.sh` build script on every push of resource.
- [ ] Automatically respond with password when required.

## Current installs run by script
### Formulae
- `brew`
- `nvm`
- `beanstalkd`
- `ola` (Open Lighting Architecture for DMX lighting)
- `node`
- `beanmaster`

### Apps
- Google Chrome
- TeamViewer
- VLC
- Caffeine
- VS Code
- iTerm 2
- Quicklook shortcuts

## System Settings Changes
- Starts `beanstalkd` on system start up
- Starts `beanmaster` daemon to monitor `beanstalkd` activity. (Accessible from `http://localhost:3000/`)
- Automatically restart if system freezes
- Avoid creating .DS_Store files on network volumes
- Disable automatically check for updates
- Disable crash reporter dialog
- Disable application resume on restart
- Disable warning on opening new app
- Disable Bluetooth seeking keyboards
- Disable Bluetooth seeking mice
- Disable screensaver
- Disable system sleep
- Disable display sleep
- Enable wake on ethernet
- Enable remote login
- Enable remote management
- Show library folder
- Show filename extensions
- Show path bar in Finder
- Show status bar in Finder
- Disable Photos.app opening when device is plugged in 
- Auto login admin account
- Set Quander Desktop wallpaper
- Enable right click
- Clean up dock and core apps to dock